from fastapi import FastAPI, UploadFile, File, HTTPException, WebSocket

from pipe_detector import PipeDetector
from plank_detector import PlankDetector

import time

app = FastAPI()

#plank_detector = PlankDetector()
pipe_detector = PipeDetector()


@app.post('/count_planks')
async def detect_planks(file: UploadFile = File(...)):
    try:
        print(file.filename)
        num_planks, bb_boxes = plank_detector.detect(file)
        return {'file_name': file.filename, 'count': num_planks, 'detections': bb_boxes}
    except Exception as e:
        # template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        # message = template.format(type(ex).__name__, ex.args)
        # return {'error': message}
        print("Exception: ", e)
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post('/count_pipes')
async def detect_pipes(file: UploadFile = File(...)):
    try:
        start_time = time.time()
        num_pipes = await pipe_detector.detect(file)
        end_time = time.time()
        print("Time taken: ", end_time - start_time)
        return {'file_name': file.filename, 'count': num_pipes}
    except Exception as e:
        # template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        # message = template.format(type(ex).__name__, ex.args)
        # return {'error': message}
        print("Exception: ", e)
        raise HTTPException(status_code=500, detail="Internal Server Error")

@app.websocket('/ws')
async def detect_pipes( websocket: WebSocket):
    try:
        await websocket.accept()
        while True:
            #file = await websocket.receive_bytes()
            #num_pipes = pipe_detector.detect(file)
            #print("Number of pipes: ", num_pipes)
            data = await websocket.receive_text()
            await websocket.send_text(f"Messege Text was: {data}")
    except Exception as e:
        print("Exception: ", e)

@app.get('/')
async def index():
    return {'msg': 'hello AI!'}
