import numpy as np
import cv2
import tensorflow as tf


class PlankDetector:
    def __init__(self):
        self.detect_fn = tf.saved_model.load('../models/saved_model_1000/')

    def detect(self, file):
        image = np.asarray(bytearray(file.file.read()), dtype="uint8")
        print("Image type: ",type(image))

        image = cv2.imdecode(image, flags=cv2.IMREAD_COLOR)
        input_tensor = np.expand_dims(image, 0)

        detections = self.detect_fn(input_tensor)
        scores = detections['detection_scores'][0].numpy()
        num_detections = int(detections['num_detections'][0].numpy())

        boxes = detections['detection_boxes'][0].numpy()
        count = 0
        better_boxes = []

        for i in range(num_detections):
            if scores[i] >= .1:
                count = count + 1
                better_boxes.append(boxes[i].tolist())

        print(count)
        return count, better_boxes
