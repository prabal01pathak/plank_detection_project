import cv2
import numpy as np
import time
import asyncio

loop = asyncio.get_event_loop()


class PipeDetector:
    def __init__(self):
        pass

    async def detect(self, file):
        start_time = time.time()
        print("Start Time: ", start_time)
        image = await np.fromstring(file.file.read(), np.uint8)
        #image = np.asarray(bytearray(file), dtype="uint8")

        image = await cv2.imdecode(image, flags=cv2.IMREAD_COLOR)

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.medianBlur(gray, 5)
        circles = await cv2.HoughCircles(gray,
                                   cv2.HOUGH_GRADIENT,
                                   minDist=30,
                                   dp=1.1,
                                   param1=550,
                                   param2=18,
                                   minRadius=50,
                                   maxRadius=58)

        count = len(circles[0, :, :])
        end_time = time.time()
        print("End Time: ", end_time)
        print(f"{__file__} Total Time: ", end_time - start_time)
        print("Total count is :", count)
        return count#, circles[0, :, :].tolist()


loop.run_until_complete(detect(loop, file))
