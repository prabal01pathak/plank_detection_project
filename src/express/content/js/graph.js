/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 544.0, "minX": 0.0, "maxY": 2394.0, "series": [{"data": [[0.0, 544.0], [0.1, 544.0], [0.2, 544.0], [0.3, 544.0], [0.4, 544.0], [0.5, 544.0], [0.6, 548.0], [0.7, 548.0], [0.8, 548.0], [0.9, 548.0], [1.0, 548.0], [1.1, 566.0], [1.2, 566.0], [1.3, 566.0], [1.4, 566.0], [1.5, 566.0], [1.6, 572.0], [1.7, 572.0], [1.8, 572.0], [1.9, 572.0], [2.0, 572.0], [2.1, 583.0], [2.2, 583.0], [2.3, 583.0], [2.4, 583.0], [2.5, 583.0], [2.6, 585.0], [2.7, 585.0], [2.8, 585.0], [2.9, 585.0], [3.0, 585.0], [3.1, 589.0], [3.2, 589.0], [3.3, 589.0], [3.4, 589.0], [3.5, 589.0], [3.6, 589.0], [3.7, 591.0], [3.8, 591.0], [3.9, 591.0], [4.0, 591.0], [4.1, 591.0], [4.2, 591.0], [4.3, 591.0], [4.4, 591.0], [4.5, 591.0], [4.6, 591.0], [4.7, 596.0], [4.8, 596.0], [4.9, 596.0], [5.0, 596.0], [5.1, 596.0], [5.2, 596.0], [5.3, 596.0], [5.4, 596.0], [5.5, 596.0], [5.6, 596.0], [5.7, 600.0], [5.8, 600.0], [5.9, 600.0], [6.0, 600.0], [6.1, 600.0], [6.2, 604.0], [6.3, 604.0], [6.4, 604.0], [6.5, 604.0], [6.6, 604.0], [6.7, 604.0], [6.8, 606.0], [6.9, 606.0], [7.0, 606.0], [7.1, 606.0], [7.2, 606.0], [7.3, 608.0], [7.4, 608.0], [7.5, 608.0], [7.6, 608.0], [7.7, 608.0], [7.8, 611.0], [7.9, 611.0], [8.0, 611.0], [8.1, 611.0], [8.2, 611.0], [8.3, 617.0], [8.4, 617.0], [8.5, 617.0], [8.6, 617.0], [8.7, 617.0], [8.8, 628.0], [8.9, 628.0], [9.0, 628.0], [9.1, 628.0], [9.2, 628.0], [9.3, 631.0], [9.4, 631.0], [9.5, 631.0], [9.6, 631.0], [9.7, 631.0], [9.8, 638.0], [9.9, 638.0], [10.0, 638.0], [10.1, 638.0], [10.2, 638.0], [10.3, 638.0], [10.4, 639.0], [10.5, 639.0], [10.6, 639.0], [10.7, 639.0], [10.8, 639.0], [10.9, 641.0], [11.0, 641.0], [11.1, 641.0], [11.2, 641.0], [11.3, 641.0], [11.4, 644.0], [11.5, 644.0], [11.6, 644.0], [11.7, 644.0], [11.8, 644.0], [11.9, 647.0], [12.0, 647.0], [12.1, 647.0], [12.2, 647.0], [12.3, 647.0], [12.4, 649.0], [12.5, 649.0], [12.6, 649.0], [12.7, 649.0], [12.8, 649.0], [12.9, 651.0], [13.0, 651.0], [13.1, 651.0], [13.2, 651.0], [13.3, 651.0], [13.4, 651.0], [13.5, 654.0], [13.6, 654.0], [13.7, 654.0], [13.8, 654.0], [13.9, 654.0], [14.0, 654.0], [14.1, 654.0], [14.2, 654.0], [14.3, 654.0], [14.4, 654.0], [14.5, 659.0], [14.6, 659.0], [14.7, 659.0], [14.8, 659.0], [14.9, 659.0], [15.0, 660.0], [15.1, 660.0], [15.2, 660.0], [15.3, 660.0], [15.4, 660.0], [15.5, 660.0], [15.6, 660.0], [15.7, 660.0], [15.8, 660.0], [15.9, 660.0], [16.0, 660.0], [16.1, 660.0], [16.2, 660.0], [16.3, 660.0], [16.4, 660.0], [16.5, 661.0], [16.6, 661.0], [16.7, 661.0], [16.8, 661.0], [16.9, 661.0], [17.0, 661.0], [17.1, 662.0], [17.2, 662.0], [17.3, 662.0], [17.4, 662.0], [17.5, 662.0], [17.6, 663.0], [17.7, 663.0], [17.8, 663.0], [17.9, 663.0], [18.0, 663.0], [18.1, 669.0], [18.2, 669.0], [18.3, 669.0], [18.4, 669.0], [18.5, 669.0], [18.6, 669.0], [18.7, 669.0], [18.8, 669.0], [18.9, 669.0], [19.0, 669.0], [19.1, 672.0], [19.2, 672.0], [19.3, 672.0], [19.4, 672.0], [19.5, 672.0], [19.6, 676.0], [19.7, 676.0], [19.8, 676.0], [19.9, 676.0], [20.0, 676.0], [20.1, 676.0], [20.2, 678.0], [20.3, 678.0], [20.4, 678.0], [20.5, 678.0], [20.6, 678.0], [20.7, 679.0], [20.8, 679.0], [20.9, 679.0], [21.0, 679.0], [21.1, 679.0], [21.2, 683.0], [21.3, 683.0], [21.4, 683.0], [21.5, 683.0], [21.6, 683.0], [21.7, 687.0], [21.8, 687.0], [21.9, 687.0], [22.0, 687.0], [22.1, 687.0], [22.2, 689.0], [22.3, 689.0], [22.4, 689.0], [22.5, 689.0], [22.6, 689.0], [22.7, 692.0], [22.8, 692.0], [22.9, 692.0], [23.0, 692.0], [23.1, 692.0], [23.2, 699.0], [23.3, 699.0], [23.4, 699.0], [23.5, 699.0], [23.6, 699.0], [23.7, 699.0], [23.8, 699.0], [23.9, 699.0], [24.0, 699.0], [24.1, 699.0], [24.2, 699.0], [24.3, 700.0], [24.4, 700.0], [24.5, 700.0], [24.6, 700.0], [24.7, 700.0], [24.8, 700.0], [24.9, 700.0], [25.0, 700.0], [25.1, 700.0], [25.2, 700.0], [25.3, 703.0], [25.4, 703.0], [25.5, 703.0], [25.6, 703.0], [25.7, 703.0], [25.8, 706.0], [25.9, 706.0], [26.0, 706.0], [26.1, 706.0], [26.2, 706.0], [26.3, 707.0], [26.4, 707.0], [26.5, 707.0], [26.6, 707.0], [26.7, 707.0], [26.8, 707.0], [26.9, 710.0], [27.0, 710.0], [27.1, 710.0], [27.2, 710.0], [27.3, 710.0], [27.4, 710.0], [27.5, 710.0], [27.6, 710.0], [27.7, 710.0], [27.8, 710.0], [27.9, 711.0], [28.0, 711.0], [28.1, 711.0], [28.2, 711.0], [28.3, 711.0], [28.4, 714.0], [28.5, 714.0], [28.6, 714.0], [28.7, 714.0], [28.8, 714.0], [28.9, 715.0], [29.0, 715.0], [29.1, 715.0], [29.2, 715.0], [29.3, 715.0], [29.4, 725.0], [29.5, 725.0], [29.6, 725.0], [29.7, 725.0], [29.8, 725.0], [29.9, 727.0], [30.0, 727.0], [30.1, 727.0], [30.2, 727.0], [30.3, 727.0], [30.4, 727.0], [30.5, 729.0], [30.6, 729.0], [30.7, 729.0], [30.8, 729.0], [30.9, 729.0], [31.0, 731.0], [31.1, 731.0], [31.2, 731.0], [31.3, 731.0], [31.4, 731.0], [31.5, 732.0], [31.6, 732.0], [31.7, 732.0], [31.8, 732.0], [31.9, 732.0], [32.0, 735.0], [32.1, 735.0], [32.2, 735.0], [32.3, 735.0], [32.4, 735.0], [32.5, 736.0], [32.6, 736.0], [32.7, 736.0], [32.8, 736.0], [32.9, 736.0], [33.0, 738.0], [33.1, 738.0], [33.2, 738.0], [33.3, 738.0], [33.4, 738.0], [33.5, 738.0], [33.6, 744.0], [33.7, 744.0], [33.8, 744.0], [33.9, 744.0], [34.0, 744.0], [34.1, 751.0], [34.2, 751.0], [34.3, 751.0], [34.4, 751.0], [34.5, 751.0], [34.6, 759.0], [34.7, 759.0], [34.8, 759.0], [34.9, 759.0], [35.0, 759.0], [35.1, 759.0], [35.2, 759.0], [35.3, 759.0], [35.4, 759.0], [35.5, 759.0], [35.6, 760.0], [35.7, 760.0], [35.8, 760.0], [35.9, 760.0], [36.0, 760.0], [36.1, 761.0], [36.2, 761.0], [36.3, 761.0], [36.4, 761.0], [36.5, 761.0], [36.6, 764.0], [36.7, 764.0], [36.8, 764.0], [36.9, 764.0], [37.0, 764.0], [37.1, 764.0], [37.2, 772.0], [37.3, 772.0], [37.4, 772.0], [37.5, 772.0], [37.6, 772.0], [37.7, 778.0], [37.8, 778.0], [37.9, 778.0], [38.0, 778.0], [38.1, 778.0], [38.2, 778.0], [38.3, 778.0], [38.4, 778.0], [38.5, 778.0], [38.6, 778.0], [38.7, 786.0], [38.8, 786.0], [38.9, 786.0], [39.0, 786.0], [39.1, 786.0], [39.2, 786.0], [39.3, 786.0], [39.4, 786.0], [39.5, 786.0], [39.6, 786.0], [39.7, 804.0], [39.8, 804.0], [39.9, 804.0], [40.0, 804.0], [40.1, 804.0], [40.2, 804.0], [40.3, 805.0], [40.4, 805.0], [40.5, 805.0], [40.6, 805.0], [40.7, 805.0], [40.8, 805.0], [40.9, 805.0], [41.0, 805.0], [41.1, 805.0], [41.2, 805.0], [41.3, 807.0], [41.4, 807.0], [41.5, 807.0], [41.6, 807.0], [41.7, 807.0], [41.8, 808.0], [41.9, 808.0], [42.0, 808.0], [42.1, 808.0], [42.2, 808.0], [42.3, 810.0], [42.4, 810.0], [42.5, 810.0], [42.6, 810.0], [42.7, 810.0], [42.8, 814.0], [42.9, 814.0], [43.0, 814.0], [43.1, 814.0], [43.2, 814.0], [43.3, 824.0], [43.4, 824.0], [43.5, 824.0], [43.6, 824.0], [43.7, 824.0], [43.8, 824.0], [43.9, 828.0], [44.0, 828.0], [44.1, 828.0], [44.2, 828.0], [44.3, 828.0], [44.4, 829.0], [44.5, 829.0], [44.6, 829.0], [44.7, 829.0], [44.8, 829.0], [44.9, 832.0], [45.0, 832.0], [45.1, 832.0], [45.2, 832.0], [45.3, 832.0], [45.4, 839.0], [45.5, 839.0], [45.6, 839.0], [45.7, 839.0], [45.8, 839.0], [45.9, 852.0], [46.0, 852.0], [46.1, 852.0], [46.2, 852.0], [46.3, 852.0], [46.4, 852.0], [46.5, 852.0], [46.6, 852.0], [46.7, 852.0], [46.8, 852.0], [46.9, 852.0], [47.0, 857.0], [47.1, 857.0], [47.2, 857.0], [47.3, 857.0], [47.4, 857.0], [47.5, 862.0], [47.6, 862.0], [47.7, 862.0], [47.8, 862.0], [47.9, 862.0], [48.0, 869.0], [48.1, 869.0], [48.2, 869.0], [48.3, 869.0], [48.4, 869.0], [48.5, 869.0], [48.6, 869.0], [48.7, 869.0], [48.8, 869.0], [48.9, 869.0], [49.0, 879.0], [49.1, 879.0], [49.2, 879.0], [49.3, 879.0], [49.4, 879.0], [49.5, 882.0], [49.6, 882.0], [49.7, 882.0], [49.8, 882.0], [49.9, 882.0], [50.0, 894.0], [50.1, 894.0], [50.2, 894.0], [50.3, 894.0], [50.4, 894.0], [50.5, 894.0], [50.6, 894.0], [50.7, 894.0], [50.8, 894.0], [50.9, 894.0], [51.0, 894.0], [51.1, 915.0], [51.2, 915.0], [51.3, 915.0], [51.4, 915.0], [51.5, 915.0], [51.6, 917.0], [51.7, 917.0], [51.8, 917.0], [51.9, 917.0], [52.0, 917.0], [52.1, 926.0], [52.2, 926.0], [52.3, 926.0], [52.4, 926.0], [52.5, 926.0], [52.6, 931.0], [52.7, 931.0], [52.8, 931.0], [52.9, 931.0], [53.0, 931.0], [53.1, 934.0], [53.2, 934.0], [53.3, 934.0], [53.4, 934.0], [53.5, 934.0], [53.6, 934.0], [53.7, 934.0], [53.8, 934.0], [53.9, 934.0], [54.0, 934.0], [54.1, 934.0], [54.2, 939.0], [54.3, 939.0], [54.4, 939.0], [54.5, 939.0], [54.6, 939.0], [54.7, 940.0], [54.8, 940.0], [54.9, 940.0], [55.0, 940.0], [55.1, 940.0], [55.2, 944.0], [55.3, 944.0], [55.4, 944.0], [55.5, 944.0], [55.6, 944.0], [55.7, 946.0], [55.8, 946.0], [55.9, 946.0], [56.0, 946.0], [56.1, 946.0], [56.2, 949.0], [56.3, 949.0], [56.4, 949.0], [56.5, 949.0], [56.6, 949.0], [56.7, 949.0], [56.8, 950.0], [56.9, 950.0], [57.0, 950.0], [57.1, 950.0], [57.2, 950.0], [57.3, 951.0], [57.4, 951.0], [57.5, 951.0], [57.6, 951.0], [57.7, 951.0], [57.8, 952.0], [57.9, 952.0], [58.0, 952.0], [58.1, 952.0], [58.2, 952.0], [58.3, 955.0], [58.4, 955.0], [58.5, 955.0], [58.6, 955.0], [58.7, 955.0], [58.8, 964.0], [58.9, 964.0], [59.0, 964.0], [59.1, 964.0], [59.2, 964.0], [59.3, 968.0], [59.4, 968.0], [59.5, 968.0], [59.6, 968.0], [59.7, 968.0], [59.8, 971.0], [59.9, 971.0], [60.0, 971.0], [60.1, 971.0], [60.2, 971.0], [60.3, 971.0], [60.4, 974.0], [60.5, 974.0], [60.6, 974.0], [60.7, 974.0], [60.8, 974.0], [60.9, 980.0], [61.0, 980.0], [61.1, 980.0], [61.2, 980.0], [61.3, 980.0], [61.4, 980.0], [61.5, 980.0], [61.6, 980.0], [61.7, 980.0], [61.8, 980.0], [61.9, 981.0], [62.0, 981.0], [62.1, 981.0], [62.2, 981.0], [62.3, 981.0], [62.4, 1017.0], [62.5, 1017.0], [62.6, 1017.0], [62.7, 1017.0], [62.8, 1017.0], [62.9, 1019.0], [63.0, 1019.0], [63.1, 1019.0], [63.2, 1019.0], [63.3, 1019.0], [63.4, 1019.0], [63.5, 1024.0], [63.6, 1024.0], [63.7, 1024.0], [63.8, 1024.0], [63.9, 1024.0], [64.0, 1025.0], [64.1, 1025.0], [64.2, 1025.0], [64.3, 1025.0], [64.4, 1025.0], [64.5, 1026.0], [64.6, 1026.0], [64.7, 1026.0], [64.8, 1026.0], [64.9, 1026.0], [65.0, 1028.0], [65.1, 1028.0], [65.2, 1028.0], [65.3, 1028.0], [65.4, 1028.0], [65.5, 1032.0], [65.6, 1032.0], [65.7, 1032.0], [65.8, 1032.0], [65.9, 1032.0], [66.0, 1033.0], [66.1, 1033.0], [66.2, 1033.0], [66.3, 1033.0], [66.4, 1033.0], [66.5, 1035.0], [66.6, 1035.0], [66.7, 1035.0], [66.8, 1035.0], [66.9, 1035.0], [67.0, 1035.0], [67.1, 1040.0], [67.2, 1040.0], [67.3, 1040.0], [67.4, 1040.0], [67.5, 1040.0], [67.6, 1041.0], [67.7, 1041.0], [67.8, 1041.0], [67.9, 1041.0], [68.0, 1041.0], [68.1, 1041.0], [68.2, 1041.0], [68.3, 1041.0], [68.4, 1041.0], [68.5, 1041.0], [68.6, 1047.0], [68.7, 1047.0], [68.8, 1047.0], [68.9, 1047.0], [69.0, 1047.0], [69.1, 1049.0], [69.2, 1049.0], [69.3, 1049.0], [69.4, 1049.0], [69.5, 1049.0], [69.6, 1060.0], [69.7, 1060.0], [69.8, 1060.0], [69.9, 1060.0], [70.0, 1060.0], [70.1, 1060.0], [70.2, 1064.0], [70.3, 1064.0], [70.4, 1064.0], [70.5, 1064.0], [70.6, 1064.0], [70.7, 1065.0], [70.8, 1065.0], [70.9, 1065.0], [71.0, 1065.0], [71.1, 1065.0], [71.2, 1066.0], [71.3, 1066.0], [71.4, 1066.0], [71.5, 1066.0], [71.6, 1066.0], [71.7, 1071.0], [71.8, 1071.0], [71.9, 1071.0], [72.0, 1071.0], [72.1, 1071.0], [72.2, 1072.0], [72.3, 1072.0], [72.4, 1072.0], [72.5, 1072.0], [72.6, 1072.0], [72.7, 1074.0], [72.8, 1074.0], [72.9, 1074.0], [73.0, 1074.0], [73.1, 1074.0], [73.2, 1083.0], [73.3, 1083.0], [73.4, 1083.0], [73.5, 1083.0], [73.6, 1083.0], [73.7, 1083.0], [73.8, 1084.0], [73.9, 1084.0], [74.0, 1084.0], [74.1, 1084.0], [74.2, 1084.0], [74.3, 1088.0], [74.4, 1088.0], [74.5, 1088.0], [74.6, 1088.0], [74.7, 1088.0], [74.8, 1090.0], [74.9, 1090.0], [75.0, 1090.0], [75.1, 1090.0], [75.2, 1090.0], [75.3, 1100.0], [75.4, 1100.0], [75.5, 1100.0], [75.6, 1100.0], [75.7, 1100.0], [75.8, 1105.0], [75.9, 1105.0], [76.0, 1105.0], [76.1, 1105.0], [76.2, 1105.0], [76.3, 1107.0], [76.4, 1107.0], [76.5, 1107.0], [76.6, 1107.0], [76.7, 1107.0], [76.8, 1107.0], [76.9, 1109.0], [77.0, 1109.0], [77.1, 1109.0], [77.2, 1109.0], [77.3, 1109.0], [77.4, 1122.0], [77.5, 1122.0], [77.6, 1122.0], [77.7, 1122.0], [77.8, 1122.0], [77.9, 1139.0], [78.0, 1139.0], [78.1, 1139.0], [78.2, 1139.0], [78.3, 1139.0], [78.4, 1145.0], [78.5, 1145.0], [78.6, 1145.0], [78.7, 1145.0], [78.8, 1145.0], [78.9, 1151.0], [79.0, 1151.0], [79.1, 1151.0], [79.2, 1151.0], [79.3, 1151.0], [79.4, 1154.0], [79.5, 1154.0], [79.6, 1154.0], [79.7, 1154.0], [79.8, 1154.0], [79.9, 1162.0], [80.0, 1162.0], [80.1, 1162.0], [80.2, 1162.0], [80.3, 1162.0], [80.4, 1162.0], [80.5, 1164.0], [80.6, 1164.0], [80.7, 1164.0], [80.8, 1164.0], [80.9, 1164.0], [81.0, 1170.0], [81.1, 1170.0], [81.2, 1170.0], [81.3, 1170.0], [81.4, 1170.0], [81.5, 1174.0], [81.6, 1174.0], [81.7, 1174.0], [81.8, 1174.0], [81.9, 1174.0], [82.0, 1176.0], [82.1, 1176.0], [82.2, 1176.0], [82.3, 1176.0], [82.4, 1176.0], [82.5, 1179.0], [82.6, 1179.0], [82.7, 1179.0], [82.8, 1179.0], [82.9, 1179.0], [83.0, 1187.0], [83.1, 1187.0], [83.2, 1187.0], [83.3, 1187.0], [83.4, 1187.0], [83.5, 1187.0], [83.6, 1193.0], [83.7, 1193.0], [83.8, 1193.0], [83.9, 1193.0], [84.0, 1193.0], [84.1, 1200.0], [84.2, 1200.0], [84.3, 1200.0], [84.4, 1200.0], [84.5, 1200.0], [84.6, 1218.0], [84.7, 1218.0], [84.8, 1218.0], [84.9, 1218.0], [85.0, 1218.0], [85.1, 1222.0], [85.2, 1222.0], [85.3, 1222.0], [85.4, 1222.0], [85.5, 1222.0], [85.6, 1225.0], [85.7, 1225.0], [85.8, 1225.0], [85.9, 1225.0], [86.0, 1225.0], [86.1, 1228.0], [86.2, 1228.0], [86.3, 1228.0], [86.4, 1228.0], [86.5, 1228.0], [86.6, 1264.0], [86.7, 1264.0], [86.8, 1264.0], [86.9, 1264.0], [87.0, 1264.0], [87.1, 1264.0], [87.2, 1279.0], [87.3, 1279.0], [87.4, 1279.0], [87.5, 1279.0], [87.6, 1279.0], [87.7, 1282.0], [87.8, 1282.0], [87.9, 1282.0], [88.0, 1282.0], [88.1, 1282.0], [88.2, 1297.0], [88.3, 1297.0], [88.4, 1297.0], [88.5, 1297.0], [88.6, 1297.0], [88.7, 1299.0], [88.8, 1299.0], [88.9, 1299.0], [89.0, 1299.0], [89.1, 1299.0], [89.2, 1305.0], [89.3, 1305.0], [89.4, 1305.0], [89.5, 1305.0], [89.6, 1305.0], [89.7, 1324.0], [89.8, 1324.0], [89.9, 1324.0], [90.0, 1324.0], [90.1, 1324.0], [90.2, 1324.0], [90.3, 1329.0], [90.4, 1329.0], [90.5, 1329.0], [90.6, 1329.0], [90.7, 1329.0], [90.8, 1351.0], [90.9, 1351.0], [91.0, 1351.0], [91.1, 1351.0], [91.2, 1351.0], [91.3, 1356.0], [91.4, 1356.0], [91.5, 1356.0], [91.6, 1356.0], [91.7, 1356.0], [91.8, 1360.0], [91.9, 1360.0], [92.0, 1360.0], [92.1, 1360.0], [92.2, 1360.0], [92.3, 1398.0], [92.4, 1398.0], [92.5, 1398.0], [92.6, 1398.0], [92.7, 1398.0], [92.8, 1406.0], [92.9, 1406.0], [93.0, 1406.0], [93.1, 1406.0], [93.2, 1406.0], [93.3, 1418.0], [93.4, 1418.0], [93.5, 1418.0], [93.6, 1418.0], [93.7, 1418.0], [93.8, 1418.0], [93.9, 1430.0], [94.0, 1430.0], [94.1, 1430.0], [94.2, 1430.0], [94.3, 1430.0], [94.4, 1520.0], [94.5, 1520.0], [94.6, 1520.0], [94.7, 1520.0], [94.8, 1520.0], [94.9, 1545.0], [95.0, 1545.0], [95.1, 1545.0], [95.2, 1545.0], [95.3, 1545.0], [95.4, 1619.0], [95.5, 1619.0], [95.6, 1619.0], [95.7, 1619.0], [95.8, 1619.0], [95.9, 1625.0], [96.0, 1625.0], [96.1, 1625.0], [96.2, 1625.0], [96.3, 1625.0], [96.4, 1700.0], [96.5, 1700.0], [96.6, 1700.0], [96.7, 1700.0], [96.8, 1700.0], [96.9, 1700.0], [97.0, 1783.0], [97.1, 1783.0], [97.2, 1783.0], [97.3, 1783.0], [97.4, 1783.0], [97.5, 1908.0], [97.6, 1908.0], [97.7, 1908.0], [97.8, 1908.0], [97.9, 1908.0], [98.0, 1919.0], [98.1, 1919.0], [98.2, 1919.0], [98.3, 1919.0], [98.4, 1919.0], [98.5, 2006.0], [98.6, 2006.0], [98.7, 2006.0], [98.8, 2006.0], [98.9, 2006.0], [99.0, 2320.0], [99.1, 2320.0], [99.2, 2320.0], [99.3, 2320.0], [99.4, 2320.0], [99.5, 2394.0], [99.6, 2394.0], [99.7, 2394.0], [99.8, 2394.0], [99.9, 2394.0], [100.0, 2394.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 500.0, "maxY": 36.0, "series": [{"data": [[2300.0, 2.0], [600.0, 36.0], [700.0, 30.0], [800.0, 22.0], [900.0, 22.0], [1000.0, 25.0], [1100.0, 17.0], [1200.0, 10.0], [1300.0, 7.0], [1400.0, 3.0], [1500.0, 2.0], [1600.0, 2.0], [1700.0, 2.0], [1900.0, 2.0], [500.0, 11.0], [2000.0, 1.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 2300.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 11.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 183.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 183.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 11.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 1.0, "minX": 1.64468682E12, "maxY": 1.0, "series": [{"data": [[1.64468682E12, 1.0], [1.644687E12, 1.0], [1.64468688E12, 1.0], [1.64468694E12, 1.0]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.644687E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 947.3402061855671, "minX": 1.0, "maxY": 947.3402061855671, "series": [{"data": [[1.0, 947.3402061855671]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[1.0, 947.3402061855671]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 1.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 47.31666666666667, "minX": 1.64468682E12, "maxY": 1026.5666666666666, "series": [{"data": [[1.64468682E12, 1026.5666666666666], [1.644687E12, 144.5], [1.64468688E12, 596.0833333333334], [1.64468694E12, 586.5]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.64468682E12, 128.03333333333333], [1.644687E12, 47.31666666666667], [1.64468688E12, 172.56666666666666], [1.64468694E12, 192.05]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.644687E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 863.6376811594205, "minX": 1.64468682E12, "maxY": 1031.3529411764705, "series": [{"data": [[1.64468682E12, 1009.9347826086957], [1.644687E12, 1031.3529411764705], [1.64468688E12, 971.016129032258], [1.64468694E12, 863.6376811594205]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.644687E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 863.623188405797, "minX": 1.64468682E12, "maxY": 1031.3529411764705, "series": [{"data": [[1.64468682E12, 1009.8913043478262], [1.644687E12, 1031.3529411764705], [1.64468688E12, 970.9838709677421], [1.64468694E12, 863.623188405797]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.644687E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 148.9275362318841, "minX": 1.64468682E12, "maxY": 184.76470588235293, "series": [{"data": [[1.64468682E12, 179.6086956521739], [1.644687E12, 184.76470588235293], [1.64468688E12, 170.53225806451613], [1.64468694E12, 148.9275362318841]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.644687E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 544.0, "minX": 1.64468682E12, "maxY": 2394.0, "series": [{"data": [[1.64468682E12, 1919.0], [1.644687E12, 2320.0], [1.64468688E12, 2394.0], [1.64468694E12, 1908.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.64468682E12, 660.0], [1.644687E12, 544.0], [1.64468688E12, 572.0], [1.64468694E12, 548.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.64468682E12, 1421.6000000000001], [1.644687E12, 2068.7999999999997], [1.64468688E12, 1288.5], [1.64468694E12, 1282.0]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.64468682E12, 1919.0], [1.644687E12, 2320.0], [1.64468688E12, 2394.0], [1.64468694E12, 1908.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.64468682E12, 1753.9499999999998], [1.644687E12, 2320.0], [1.64468688E12, 1587.0499999999988], [1.64468694E12, 1424.5]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.644687E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 745.5, "minX": 1.0, "maxY": 949.5, "series": [{"data": [[1.0, 949.5], [2.0, 745.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 2.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 745.5, "minX": 1.0, "maxY": 949.5, "series": [{"data": [[1.0, 949.5], [2.0, 745.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 2.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.26666666666666666, "minX": 1.64468682E12, "maxY": 1.15, "series": [{"data": [[1.64468682E12, 0.7833333333333333], [1.644687E12, 0.26666666666666666], [1.64468688E12, 1.0333333333333334], [1.64468694E12, 1.15]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.644687E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.2833333333333333, "minX": 1.64468682E12, "maxY": 1.15, "series": [{"data": [[1.64468682E12, 0.7666666666666667], [1.644687E12, 0.2833333333333333], [1.64468688E12, 1.0333333333333334], [1.64468694E12, 1.15]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.644687E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.2833333333333333, "minX": 1.64468682E12, "maxY": 1.15, "series": [{"data": [[1.64468682E12, 0.7666666666666667], [1.644687E12, 0.2833333333333333], [1.64468688E12, 1.0333333333333334], [1.64468694E12, 1.15]], "isOverall": false, "label": "HTTP Request-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.644687E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.2833333333333333, "minX": 1.64468682E12, "maxY": 1.15, "series": [{"data": [[1.64468682E12, 0.7666666666666667], [1.644687E12, 0.2833333333333333], [1.64468688E12, 1.0333333333333334], [1.64468694E12, 1.15]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.644687E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

