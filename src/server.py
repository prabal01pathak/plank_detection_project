import grpc
from concurrent import futures
import time

import app_pb2
import app_pb2_grpc

from pipe_detector import PipeDetector
from plank_detector import PlankDetector


# Initilize the pipe detector and plank detector
plank_detector = PlankDetector()
pipe_detector = PipeDetector()


# Plank Detection
class PlankDetectorSerivcer(app_pb2_grpc.PlankDetectorServiceServicer):

    def DetectPlanks(self, request, context):
        print("Plank Detection")

        start_time = time.time()
        detected_planks=plank_detector.detect(request.image)
        end_time = time.time()

        print("Plank Detection Time: ", end_time - start_time)
        print(detected_planks[0])

        return app_pb2.DetectorResult(file_name="None",num = detected_planks[0], bounding_box = str(detected_planks[1]))


# Pipe Detection
class PipeDetectorServicer(app_pb2_grpc.PipeDetectorServiceServicer):

    def DetectPipes(self, request, context):
        start_time = time.time()
        detected_pipes=pipe_detector.detect(request.image)
        end_time = time.time()

        print(f"Pipe Detection Time: {end_time - start_time}")
        print(detected_pipes[0])

        return app_pb2.DetectorResult(file_name=f"Hello, PipeDetectorServicer",num=detected_pipes[0],bounding_box= str(detected_pipes[1]))



def serve():
    MAX_MESSAGE_LENGTH = 256*1024*1024 # can be set according to specific needs, here is set to 256M
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=(('grpc.max_send_message_length', MAX_MESSAGE_LENGTH), ('grpc.max_receive_message_length', MAX_MESSAGE_LENGTH)))
    #server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    app_pb2_grpc.add_PlankDetectorServiceServicer_to_server(PlankDetectorSerivcer(), server)
    app_pb2_grpc.add_PipeDetectorServiceServicer_to_server(PipeDetectorServicer(), server)

    server.add_insecure_port('[::]:50051')
    print("Starting server on port 50051")
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
