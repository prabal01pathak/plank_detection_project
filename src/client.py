import grpc

import app_pb2
import app_pb2_grpc

import time
import sys

channel = grpc.insecure_channel('localhost:50051')

def plank_detection(filename="planks.png"):
    stub = app_pb2_grpc.PlankDetectorServiceStub(channel)
    start_time = time.time()
    req = app_pb2.Detector(image=open(filename, 'rb').read())
    response = stub.DetectPlanks(req)
    end_time = time.time()

    print(response)
    print("Time taken: {}".format(end_time - start_time))
    return response


def pipe_detection(filename="pipes.png"):
    stub = app_pb2_grpc.PipeDetectorServiceStub(channel)
    start_time = time.time()
    req = app_pb2.Detector(image=open(filename, 'rb').read())
    response = stub.DetectPipes(req)
    end_time = time.time()

    print(response)
    print("Time taken: {}".format(end_time - start_time))
    return response

if __name__=="__main__":

    plank_detection()
    #pipe_detection()
