FROM python:3.6
 

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt


RUN pip install  numpy==1.19.5
RUN pip install --upgrade pip
RUN pip install tensorflow-cpu==2.6.0
RUN pip install --upgrade -r /code/requirements.txt

COPY ./ /code/

EXPOSE 80

CMD ["uvicorn", "src.main:app","--host","0.0.0.0", "--port", "80"]
