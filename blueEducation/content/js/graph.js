/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 287.0, "minX": 0.0, "maxY": 6705.0, "series": [{"data": [[0.0, 287.0], [0.1, 287.0], [0.2, 287.0], [0.3, 287.0], [0.4, 287.0], [0.5, 287.0], [0.6, 287.0], [0.7, 289.0], [0.8, 289.0], [0.9, 289.0], [1.0, 289.0], [1.1, 289.0], [1.2, 289.0], [1.3, 294.0], [1.4, 294.0], [1.5, 294.0], [1.6, 294.0], [1.7, 294.0], [1.8, 294.0], [1.9, 295.0], [2.0, 295.0], [2.1, 295.0], [2.2, 295.0], [2.3, 295.0], [2.4, 295.0], [2.5, 300.0], [2.6, 300.0], [2.7, 300.0], [2.8, 300.0], [2.9, 300.0], [3.0, 300.0], [3.1, 300.0], [3.2, 303.0], [3.3, 303.0], [3.4, 303.0], [3.5, 303.0], [3.6, 303.0], [3.7, 303.0], [3.8, 305.0], [3.9, 305.0], [4.0, 305.0], [4.1, 305.0], [4.2, 305.0], [4.3, 305.0], [4.4, 305.0], [4.5, 305.0], [4.6, 305.0], [4.7, 305.0], [4.8, 305.0], [4.9, 305.0], [5.0, 305.0], [5.1, 305.0], [5.2, 305.0], [5.3, 305.0], [5.4, 305.0], [5.5, 305.0], [5.6, 305.0], [5.7, 311.0], [5.8, 311.0], [5.9, 311.0], [6.0, 311.0], [6.1, 311.0], [6.2, 311.0], [6.3, 313.0], [6.4, 313.0], [6.5, 313.0], [6.6, 313.0], [6.7, 313.0], [6.8, 313.0], [6.9, 320.0], [7.0, 320.0], [7.1, 320.0], [7.2, 320.0], [7.3, 320.0], [7.4, 320.0], [7.5, 320.0], [7.6, 320.0], [7.7, 320.0], [7.8, 320.0], [7.9, 320.0], [8.0, 320.0], [8.1, 320.0], [8.2, 321.0], [8.3, 321.0], [8.4, 321.0], [8.5, 321.0], [8.6, 321.0], [8.7, 321.0], [8.8, 326.0], [8.9, 326.0], [9.0, 326.0], [9.1, 326.0], [9.2, 326.0], [9.3, 326.0], [9.4, 328.0], [9.5, 328.0], [9.6, 328.0], [9.7, 328.0], [9.8, 328.0], [9.9, 328.0], [10.0, 330.0], [10.1, 330.0], [10.2, 330.0], [10.3, 330.0], [10.4, 330.0], [10.5, 330.0], [10.6, 330.0], [10.7, 333.0], [10.8, 333.0], [10.9, 333.0], [11.0, 333.0], [11.1, 333.0], [11.2, 333.0], [11.3, 334.0], [11.4, 334.0], [11.5, 334.0], [11.6, 334.0], [11.7, 334.0], [11.8, 334.0], [11.9, 337.0], [12.0, 337.0], [12.1, 337.0], [12.2, 337.0], [12.3, 337.0], [12.4, 337.0], [12.5, 339.0], [12.6, 339.0], [12.7, 339.0], [12.8, 339.0], [12.9, 339.0], [13.0, 339.0], [13.1, 339.0], [13.2, 339.0], [13.3, 339.0], [13.4, 339.0], [13.5, 339.0], [13.6, 339.0], [13.7, 339.0], [13.8, 341.0], [13.9, 341.0], [14.0, 341.0], [14.1, 341.0], [14.2, 341.0], [14.3, 341.0], [14.4, 356.0], [14.5, 356.0], [14.6, 356.0], [14.7, 356.0], [14.8, 356.0], [14.9, 356.0], [15.0, 384.0], [15.1, 384.0], [15.2, 384.0], [15.3, 384.0], [15.4, 384.0], [15.5, 384.0], [15.6, 384.0], [15.7, 387.0], [15.8, 387.0], [15.9, 387.0], [16.0, 387.0], [16.1, 387.0], [16.2, 387.0], [16.3, 389.0], [16.4, 389.0], [16.5, 389.0], [16.6, 389.0], [16.7, 389.0], [16.8, 389.0], [16.9, 398.0], [17.0, 398.0], [17.1, 398.0], [17.2, 398.0], [17.3, 398.0], [17.4, 398.0], [17.5, 403.0], [17.6, 403.0], [17.7, 403.0], [17.8, 403.0], [17.9, 403.0], [18.0, 403.0], [18.1, 403.0], [18.2, 408.0], [18.3, 408.0], [18.4, 408.0], [18.5, 408.0], [18.6, 408.0], [18.7, 408.0], [18.8, 418.0], [18.9, 418.0], [19.0, 418.0], [19.1, 418.0], [19.2, 418.0], [19.3, 418.0], [19.4, 432.0], [19.5, 432.0], [19.6, 432.0], [19.7, 432.0], [19.8, 432.0], [19.9, 432.0], [20.0, 434.0], [20.1, 434.0], [20.2, 434.0], [20.3, 434.0], [20.4, 434.0], [20.5, 434.0], [20.6, 434.0], [20.7, 438.0], [20.8, 438.0], [20.9, 438.0], [21.0, 438.0], [21.1, 438.0], [21.2, 438.0], [21.3, 471.0], [21.4, 471.0], [21.5, 471.0], [21.6, 471.0], [21.7, 471.0], [21.8, 471.0], [21.9, 473.0], [22.0, 473.0], [22.1, 473.0], [22.2, 473.0], [22.3, 473.0], [22.4, 473.0], [22.5, 474.0], [22.6, 474.0], [22.7, 474.0], [22.8, 474.0], [22.9, 474.0], [23.0, 474.0], [23.1, 474.0], [23.2, 491.0], [23.3, 491.0], [23.4, 491.0], [23.5, 491.0], [23.6, 491.0], [23.7, 491.0], [23.8, 544.0], [23.9, 544.0], [24.0, 544.0], [24.1, 544.0], [24.2, 544.0], [24.3, 544.0], [24.4, 548.0], [24.5, 548.0], [24.6, 548.0], [24.7, 548.0], [24.8, 548.0], [24.9, 548.0], [25.0, 558.0], [25.1, 558.0], [25.2, 558.0], [25.3, 558.0], [25.4, 558.0], [25.5, 558.0], [25.6, 558.0], [25.7, 560.0], [25.8, 560.0], [25.9, 560.0], [26.0, 560.0], [26.1, 560.0], [26.2, 560.0], [26.3, 560.0], [26.4, 560.0], [26.5, 560.0], [26.6, 560.0], [26.7, 560.0], [26.8, 560.0], [26.9, 560.0], [27.0, 560.0], [27.1, 560.0], [27.2, 560.0], [27.3, 560.0], [27.4, 560.0], [27.5, 570.0], [27.6, 570.0], [27.7, 570.0], [27.8, 570.0], [27.9, 570.0], [28.0, 570.0], [28.1, 570.0], [28.2, 577.0], [28.3, 577.0], [28.4, 577.0], [28.5, 577.0], [28.6, 577.0], [28.7, 577.0], [28.8, 581.0], [28.9, 581.0], [29.0, 581.0], [29.1, 581.0], [29.2, 581.0], [29.3, 581.0], [29.4, 582.0], [29.5, 582.0], [29.6, 582.0], [29.7, 582.0], [29.8, 582.0], [29.9, 582.0], [30.0, 586.0], [30.1, 586.0], [30.2, 586.0], [30.3, 586.0], [30.4, 586.0], [30.5, 586.0], [30.6, 586.0], [30.7, 587.0], [30.8, 587.0], [30.9, 587.0], [31.0, 587.0], [31.1, 587.0], [31.2, 587.0], [31.3, 590.0], [31.4, 590.0], [31.5, 590.0], [31.6, 590.0], [31.7, 590.0], [31.8, 590.0], [31.9, 617.0], [32.0, 617.0], [32.1, 617.0], [32.2, 617.0], [32.3, 617.0], [32.4, 617.0], [32.5, 637.0], [32.6, 637.0], [32.7, 637.0], [32.8, 637.0], [32.9, 637.0], [33.0, 637.0], [33.1, 637.0], [33.2, 640.0], [33.3, 640.0], [33.4, 640.0], [33.5, 640.0], [33.6, 640.0], [33.7, 640.0], [33.8, 642.0], [33.9, 642.0], [34.0, 642.0], [34.1, 642.0], [34.2, 642.0], [34.3, 642.0], [34.4, 650.0], [34.5, 650.0], [34.6, 650.0], [34.7, 650.0], [34.8, 650.0], [34.9, 650.0], [35.0, 653.0], [35.1, 653.0], [35.2, 653.0], [35.3, 653.0], [35.4, 653.0], [35.5, 653.0], [35.6, 653.0], [35.7, 660.0], [35.8, 660.0], [35.9, 660.0], [36.0, 660.0], [36.1, 660.0], [36.2, 660.0], [36.3, 696.0], [36.4, 696.0], [36.5, 696.0], [36.6, 696.0], [36.7, 696.0], [36.8, 696.0], [36.9, 699.0], [37.0, 699.0], [37.1, 699.0], [37.2, 699.0], [37.3, 699.0], [37.4, 699.0], [37.5, 711.0], [37.6, 711.0], [37.7, 711.0], [37.8, 711.0], [37.9, 711.0], [38.0, 711.0], [38.1, 711.0], [38.2, 852.0], [38.3, 852.0], [38.4, 852.0], [38.5, 852.0], [38.6, 852.0], [38.7, 852.0], [38.8, 869.0], [38.9, 869.0], [39.0, 869.0], [39.1, 869.0], [39.2, 869.0], [39.3, 869.0], [39.4, 895.0], [39.5, 895.0], [39.6, 895.0], [39.7, 895.0], [39.8, 895.0], [39.9, 895.0], [40.0, 1138.0], [40.1, 1138.0], [40.2, 1138.0], [40.3, 1138.0], [40.4, 1138.0], [40.5, 1138.0], [40.6, 1138.0], [40.7, 1226.0], [40.8, 1226.0], [40.9, 1226.0], [41.0, 1226.0], [41.1, 1226.0], [41.2, 1226.0], [41.3, 1288.0], [41.4, 1288.0], [41.5, 1288.0], [41.6, 1288.0], [41.7, 1288.0], [41.8, 1288.0], [41.9, 1305.0], [42.0, 1305.0], [42.1, 1305.0], [42.2, 1305.0], [42.3, 1305.0], [42.4, 1305.0], [42.5, 1309.0], [42.6, 1309.0], [42.7, 1309.0], [42.8, 1309.0], [42.9, 1309.0], [43.0, 1309.0], [43.1, 1309.0], [43.2, 1310.0], [43.3, 1310.0], [43.4, 1310.0], [43.5, 1310.0], [43.6, 1310.0], [43.7, 1310.0], [43.8, 1318.0], [43.9, 1318.0], [44.0, 1318.0], [44.1, 1318.0], [44.2, 1318.0], [44.3, 1318.0], [44.4, 1373.0], [44.5, 1373.0], [44.6, 1373.0], [44.7, 1373.0], [44.8, 1373.0], [44.9, 1373.0], [45.0, 1374.0], [45.1, 1374.0], [45.2, 1374.0], [45.3, 1374.0], [45.4, 1374.0], [45.5, 1374.0], [45.6, 1374.0], [45.7, 1432.0], [45.8, 1432.0], [45.9, 1432.0], [46.0, 1432.0], [46.1, 1432.0], [46.2, 1432.0], [46.3, 1433.0], [46.4, 1433.0], [46.5, 1433.0], [46.6, 1433.0], [46.7, 1433.0], [46.8, 1433.0], [46.9, 1443.0], [47.0, 1443.0], [47.1, 1443.0], [47.2, 1443.0], [47.3, 1443.0], [47.4, 1443.0], [47.5, 1550.0], [47.6, 1550.0], [47.7, 1550.0], [47.8, 1550.0], [47.9, 1550.0], [48.0, 1550.0], [48.1, 1550.0], [48.2, 1550.0], [48.3, 1550.0], [48.4, 1550.0], [48.5, 1550.0], [48.6, 1550.0], [48.7, 1550.0], [48.8, 1553.0], [48.9, 1553.0], [49.0, 1553.0], [49.1, 1553.0], [49.2, 1553.0], [49.3, 1553.0], [49.4, 1558.0], [49.5, 1558.0], [49.6, 1558.0], [49.7, 1558.0], [49.8, 1558.0], [49.9, 1558.0], [50.0, 1595.0], [50.1, 1595.0], [50.2, 1595.0], [50.3, 1595.0], [50.4, 1595.0], [50.5, 1595.0], [50.6, 1595.0], [50.7, 1601.0], [50.8, 1601.0], [50.9, 1601.0], [51.0, 1601.0], [51.1, 1601.0], [51.2, 1601.0], [51.3, 1610.0], [51.4, 1610.0], [51.5, 1610.0], [51.6, 1610.0], [51.7, 1610.0], [51.8, 1610.0], [51.9, 1615.0], [52.0, 1615.0], [52.1, 1615.0], [52.2, 1615.0], [52.3, 1615.0], [52.4, 1615.0], [52.5, 1622.0], [52.6, 1622.0], [52.7, 1622.0], [52.8, 1622.0], [52.9, 1622.0], [53.0, 1622.0], [53.1, 1622.0], [53.2, 1624.0], [53.3, 1624.0], [53.4, 1624.0], [53.5, 1624.0], [53.6, 1624.0], [53.7, 1624.0], [53.8, 1638.0], [53.9, 1638.0], [54.0, 1638.0], [54.1, 1638.0], [54.2, 1638.0], [54.3, 1638.0], [54.4, 1643.0], [54.5, 1643.0], [54.6, 1643.0], [54.7, 1643.0], [54.8, 1643.0], [54.9, 1643.0], [55.0, 1674.0], [55.1, 1674.0], [55.2, 1674.0], [55.3, 1674.0], [55.4, 1674.0], [55.5, 1674.0], [55.6, 1674.0], [55.7, 1708.0], [55.8, 1708.0], [55.9, 1708.0], [56.0, 1708.0], [56.1, 1708.0], [56.2, 1708.0], [56.3, 1715.0], [56.4, 1715.0], [56.5, 1715.0], [56.6, 1715.0], [56.7, 1715.0], [56.8, 1715.0], [56.9, 1741.0], [57.0, 1741.0], [57.1, 1741.0], [57.2, 1741.0], [57.3, 1741.0], [57.4, 1741.0], [57.5, 1764.0], [57.6, 1764.0], [57.7, 1764.0], [57.8, 1764.0], [57.9, 1764.0], [58.0, 1764.0], [58.1, 1764.0], [58.2, 1782.0], [58.3, 1782.0], [58.4, 1782.0], [58.5, 1782.0], [58.6, 1782.0], [58.7, 1782.0], [58.8, 1794.0], [58.9, 1794.0], [59.0, 1794.0], [59.1, 1794.0], [59.2, 1794.0], [59.3, 1794.0], [59.4, 1795.0], [59.5, 1795.0], [59.6, 1795.0], [59.7, 1795.0], [59.8, 1795.0], [59.9, 1795.0], [60.0, 1831.0], [60.1, 1831.0], [60.2, 1831.0], [60.3, 1831.0], [60.4, 1831.0], [60.5, 1831.0], [60.6, 1831.0], [60.7, 1843.0], [60.8, 1843.0], [60.9, 1843.0], [61.0, 1843.0], [61.1, 1843.0], [61.2, 1843.0], [61.3, 1848.0], [61.4, 1848.0], [61.5, 1848.0], [61.6, 1848.0], [61.7, 1848.0], [61.8, 1848.0], [61.9, 1848.0], [62.0, 1848.0], [62.1, 1848.0], [62.2, 1848.0], [62.3, 1848.0], [62.4, 1848.0], [62.5, 1865.0], [62.6, 1865.0], [62.7, 1865.0], [62.8, 1865.0], [62.9, 1865.0], [63.0, 1865.0], [63.1, 1865.0], [63.2, 1873.0], [63.3, 1873.0], [63.4, 1873.0], [63.5, 1873.0], [63.6, 1873.0], [63.7, 1873.0], [63.8, 1873.0], [63.9, 1873.0], [64.0, 1873.0], [64.1, 1873.0], [64.2, 1873.0], [64.3, 1873.0], [64.4, 1874.0], [64.5, 1874.0], [64.6, 1874.0], [64.7, 1874.0], [64.8, 1874.0], [64.9, 1874.0], [65.0, 1882.0], [65.1, 1882.0], [65.2, 1882.0], [65.3, 1882.0], [65.4, 1882.0], [65.5, 1882.0], [65.6, 1882.0], [65.7, 1902.0], [65.8, 1902.0], [65.9, 1902.0], [66.0, 1902.0], [66.1, 1902.0], [66.2, 1902.0], [66.3, 1914.0], [66.4, 1914.0], [66.5, 1914.0], [66.6, 1914.0], [66.7, 1914.0], [66.8, 1914.0], [66.9, 1917.0], [67.0, 1917.0], [67.1, 1917.0], [67.2, 1917.0], [67.3, 1917.0], [67.4, 1917.0], [67.5, 1919.0], [67.6, 1919.0], [67.7, 1919.0], [67.8, 1919.0], [67.9, 1919.0], [68.0, 1919.0], [68.1, 1919.0], [68.2, 1920.0], [68.3, 1920.0], [68.4, 1920.0], [68.5, 1920.0], [68.6, 1920.0], [68.7, 1920.0], [68.8, 1922.0], [68.9, 1922.0], [69.0, 1922.0], [69.1, 1922.0], [69.2, 1922.0], [69.3, 1922.0], [69.4, 1928.0], [69.5, 1928.0], [69.6, 1928.0], [69.7, 1928.0], [69.8, 1928.0], [69.9, 1928.0], [70.0, 1929.0], [70.1, 1929.0], [70.2, 1929.0], [70.3, 1929.0], [70.4, 1929.0], [70.5, 1929.0], [70.6, 1929.0], [70.7, 1934.0], [70.8, 1934.0], [70.9, 1934.0], [71.0, 1934.0], [71.1, 1934.0], [71.2, 1934.0], [71.3, 1934.0], [71.4, 1934.0], [71.5, 1934.0], [71.6, 1934.0], [71.7, 1934.0], [71.8, 1934.0], [71.9, 1946.0], [72.0, 1946.0], [72.1, 1946.0], [72.2, 1946.0], [72.3, 1946.0], [72.4, 1946.0], [72.5, 1952.0], [72.6, 1952.0], [72.7, 1952.0], [72.8, 1952.0], [72.9, 1952.0], [73.0, 1952.0], [73.1, 1952.0], [73.2, 1959.0], [73.3, 1959.0], [73.4, 1959.0], [73.5, 1959.0], [73.6, 1959.0], [73.7, 1959.0], [73.8, 1964.0], [73.9, 1964.0], [74.0, 1964.0], [74.1, 1964.0], [74.2, 1964.0], [74.3, 1964.0], [74.4, 1968.0], [74.5, 1968.0], [74.6, 1968.0], [74.7, 1968.0], [74.8, 1968.0], [74.9, 1968.0], [75.0, 1972.0], [75.1, 1972.0], [75.2, 1972.0], [75.3, 1972.0], [75.4, 1972.0], [75.5, 1972.0], [75.6, 1972.0], [75.7, 1987.0], [75.8, 1987.0], [75.9, 1987.0], [76.0, 1987.0], [76.1, 1987.0], [76.2, 1987.0], [76.3, 1989.0], [76.4, 1989.0], [76.5, 1989.0], [76.6, 1989.0], [76.7, 1989.0], [76.8, 1989.0], [76.9, 1991.0], [77.0, 1991.0], [77.1, 1991.0], [77.2, 1991.0], [77.3, 1991.0], [77.4, 1991.0], [77.5, 2003.0], [77.6, 2003.0], [77.7, 2003.0], [77.8, 2003.0], [77.9, 2003.0], [78.0, 2003.0], [78.1, 2003.0], [78.2, 2005.0], [78.3, 2005.0], [78.4, 2005.0], [78.5, 2005.0], [78.6, 2005.0], [78.7, 2005.0], [78.8, 2069.0], [78.9, 2069.0], [79.0, 2069.0], [79.1, 2069.0], [79.2, 2069.0], [79.3, 2069.0], [79.4, 2072.0], [79.5, 2072.0], [79.6, 2072.0], [79.7, 2072.0], [79.8, 2072.0], [79.9, 2072.0], [80.0, 2078.0], [80.1, 2078.0], [80.2, 2078.0], [80.3, 2078.0], [80.4, 2078.0], [80.5, 2078.0], [80.6, 2078.0], [80.7, 2080.0], [80.8, 2080.0], [80.9, 2080.0], [81.0, 2080.0], [81.1, 2080.0], [81.2, 2080.0], [81.3, 2084.0], [81.4, 2084.0], [81.5, 2084.0], [81.6, 2084.0], [81.7, 2084.0], [81.8, 2084.0], [81.9, 2088.0], [82.0, 2088.0], [82.1, 2088.0], [82.2, 2088.0], [82.3, 2088.0], [82.4, 2088.0], [82.5, 2089.0], [82.6, 2089.0], [82.7, 2089.0], [82.8, 2089.0], [82.9, 2089.0], [83.0, 2089.0], [83.1, 2089.0], [83.2, 2116.0], [83.3, 2116.0], [83.4, 2116.0], [83.5, 2116.0], [83.6, 2116.0], [83.7, 2116.0], [83.8, 2131.0], [83.9, 2131.0], [84.0, 2131.0], [84.1, 2131.0], [84.2, 2131.0], [84.3, 2131.0], [84.4, 2144.0], [84.5, 2144.0], [84.6, 2144.0], [84.7, 2144.0], [84.8, 2144.0], [84.9, 2144.0], [85.0, 2155.0], [85.1, 2155.0], [85.2, 2155.0], [85.3, 2155.0], [85.4, 2155.0], [85.5, 2155.0], [85.6, 2155.0], [85.7, 2245.0], [85.8, 2245.0], [85.9, 2245.0], [86.0, 2245.0], [86.1, 2245.0], [86.2, 2245.0], [86.3, 2268.0], [86.4, 2268.0], [86.5, 2268.0], [86.6, 2268.0], [86.7, 2268.0], [86.8, 2268.0], [86.9, 2268.0], [87.0, 2268.0], [87.1, 2268.0], [87.2, 2268.0], [87.3, 2268.0], [87.4, 2268.0], [87.5, 2317.0], [87.6, 2317.0], [87.7, 2317.0], [87.8, 2317.0], [87.9, 2317.0], [88.0, 2317.0], [88.1, 2317.0], [88.2, 2330.0], [88.3, 2330.0], [88.4, 2330.0], [88.5, 2330.0], [88.6, 2330.0], [88.7, 2330.0], [88.8, 2385.0], [88.9, 2385.0], [89.0, 2385.0], [89.1, 2385.0], [89.2, 2385.0], [89.3, 2385.0], [89.4, 2453.0], [89.5, 2453.0], [89.6, 2453.0], [89.7, 2453.0], [89.8, 2453.0], [89.9, 2453.0], [90.0, 2465.0], [90.1, 2465.0], [90.2, 2465.0], [90.3, 2465.0], [90.4, 2465.0], [90.5, 2465.0], [90.6, 2465.0], [90.7, 2470.0], [90.8, 2470.0], [90.9, 2470.0], [91.0, 2470.0], [91.1, 2470.0], [91.2, 2470.0], [91.3, 2474.0], [91.4, 2474.0], [91.5, 2474.0], [91.6, 2474.0], [91.7, 2474.0], [91.8, 2474.0], [91.9, 2495.0], [92.0, 2495.0], [92.1, 2495.0], [92.2, 2495.0], [92.3, 2495.0], [92.4, 2495.0], [92.5, 2651.0], [92.6, 2651.0], [92.7, 2651.0], [92.8, 2651.0], [92.9, 2651.0], [93.0, 2651.0], [93.1, 2651.0], [93.2, 2651.0], [93.3, 2651.0], [93.4, 2651.0], [93.5, 2651.0], [93.6, 2651.0], [93.7, 2651.0], [93.8, 2708.0], [93.9, 2708.0], [94.0, 2708.0], [94.1, 2708.0], [94.2, 2708.0], [94.3, 2708.0], [94.4, 2714.0], [94.5, 2714.0], [94.6, 2714.0], [94.7, 2714.0], [94.8, 2714.0], [94.9, 2714.0], [95.0, 3456.0], [95.1, 3456.0], [95.2, 3456.0], [95.3, 3456.0], [95.4, 3456.0], [95.5, 3456.0], [95.6, 3456.0], [95.7, 4207.0], [95.8, 4207.0], [95.9, 4207.0], [96.0, 4207.0], [96.1, 4207.0], [96.2, 4207.0], [96.3, 5406.0], [96.4, 5406.0], [96.5, 5406.0], [96.6, 5406.0], [96.7, 5406.0], [96.8, 5406.0], [96.9, 5415.0], [97.0, 5415.0], [97.1, 5415.0], [97.2, 5415.0], [97.3, 5415.0], [97.4, 5415.0], [97.5, 5420.0], [97.6, 5420.0], [97.7, 5420.0], [97.8, 5420.0], [97.9, 5420.0], [98.0, 5420.0], [98.1, 5420.0], [98.2, 5421.0], [98.3, 5421.0], [98.4, 5421.0], [98.5, 5421.0], [98.6, 5421.0], [98.7, 5421.0], [98.8, 5740.0], [98.9, 5740.0], [99.0, 5740.0], [99.1, 5740.0], [99.2, 5740.0], [99.3, 5740.0], [99.4, 6705.0], [99.5, 6705.0], [99.6, 6705.0], [99.7, 6705.0], [99.8, 6705.0], [99.9, 6705.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 200.0, "maxY": 24.0, "series": [{"data": [[600.0, 9.0], [700.0, 1.0], [800.0, 3.0], [1100.0, 1.0], [1200.0, 2.0], [1300.0, 6.0], [1400.0, 3.0], [1500.0, 5.0], [1600.0, 8.0], [1700.0, 7.0], [1800.0, 9.0], [1900.0, 19.0], [2000.0, 9.0], [2100.0, 4.0], [2200.0, 3.0], [2300.0, 3.0], [2400.0, 5.0], [2600.0, 2.0], [2700.0, 2.0], [200.0, 4.0], [3400.0, 1.0], [4200.0, 1.0], [300.0, 24.0], [5400.0, 4.0], [5700.0, 1.0], [400.0, 10.0], [6700.0, 1.0], [500.0, 13.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 6700.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 38.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 84.0, "series": [{"data": [[0.0, 38.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 38.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 84.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 3.5, "minX": 1.6447635E12, "maxY": 7.906666666666668, "series": [{"data": [[1.6447635E12, 7.906666666666668], [1.64476356E12, 3.5]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64476356E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 303.0, "minX": 1.0, "maxY": 5740.0, "series": [{"data": [[8.0, 1264.554744525548], [4.0, 1389.1666666666667], [2.0, 1288.0], [1.0, 1601.0], [5.0, 1831.0], [6.0, 5740.0], [3.0, 303.0], [7.0, 3871.3333333333335]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[7.631250000000001, 1492.4875000000002]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 8.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 22.0, "minX": 1.6447635E12, "maxY": 40922.5, "series": [{"data": [[1.6447635E12, 40922.5], [1.64476356E12, 2728.1666666666665]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.6447635E12, 330.0], [1.64476356E12, 22.0]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64476356E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 1335.8000000000002, "minX": 1.6447635E12, "maxY": 1502.9333333333338, "series": [{"data": [[1.6447635E12, 1502.9333333333338], [1.64476356E12, 1335.8000000000002]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64476356E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 1274.0, "minX": 1.6447635E12, "maxY": 1443.9799999999998, "series": [{"data": [[1.6447635E12, 1443.9799999999998], [1.64476356E12, 1274.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64476356E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 801.2, "minX": 1.6447635E12, "maxY": 902.8333333333333, "series": [{"data": [[1.6447635E12, 902.8333333333333], [1.64476356E12, 801.2]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64476356E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 287.0, "minX": 1.6447635E12, "maxY": 6705.0, "series": [{"data": [[1.6447635E12, 6705.0], [1.64476356E12, 2144.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.6447635E12, 287.0], [1.64476356E12, 294.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.6447635E12, 2469.5], [1.64476356E12, 2116.9]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.6447635E12, 6212.850000000009], [1.64476356E12, 2144.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.6447635E12, 3793.9499999999916], [1.64476356E12, 2144.0]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64476356E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 560.0, "minX": 1.0, "maxY": 2086.5, "series": [{"data": [[4.0, 2086.5], [8.0, 1206.0], [1.0, 1993.0], [10.0, 1466.0], [5.0, 560.0], [11.0, 1616.5], [3.0, 1623.0], [6.0, 1134.0], [14.0, 1388.0], [7.0, 1928.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 14.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 556.5, "minX": 1.0, "maxY": 2080.0, "series": [{"data": [[4.0, 2080.0], [8.0, 1164.0], [1.0, 1838.5], [10.0, 1463.5], [5.0, 556.5], [11.0, 1616.0], [3.0, 1623.0], [6.0, 1131.0], [14.0, 1388.0], [7.0, 1926.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 14.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.08333333333333333, "minX": 1.6447635E12, "maxY": 2.5833333333333335, "series": [{"data": [[1.6447635E12, 2.5833333333333335], [1.64476356E12, 0.08333333333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64476356E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.6447635E12, "maxY": 2.5, "series": [{"data": [[1.6447635E12, 2.5], [1.64476356E12, 0.16666666666666666]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.64476356E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.6447635E12, "maxY": 2.5, "series": [{"data": [[1.6447635E12, 2.5], [1.64476356E12, 0.16666666666666666]], "isOverall": false, "label": "HTTP Request-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64476356E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.16666666666666666, "minX": 1.6447635E12, "maxY": 2.5, "series": [{"data": [[1.6447635E12, 2.5], [1.64476356E12, 0.16666666666666666]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.64476356E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

