/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 449.0, "minX": 0.0, "maxY": 2155.0, "series": [{"data": [[0.0, 449.0], [0.1, 449.0], [0.2, 449.0], [0.3, 449.0], [0.4, 449.0], [0.5, 449.0], [0.6, 449.0], [0.7, 453.0], [0.8, 453.0], [0.9, 453.0], [1.0, 453.0], [1.1, 453.0], [1.2, 453.0], [1.3, 482.0], [1.4, 482.0], [1.5, 482.0], [1.6, 482.0], [1.7, 482.0], [1.8, 482.0], [1.9, 490.0], [2.0, 490.0], [2.1, 490.0], [2.2, 490.0], [2.3, 490.0], [2.4, 490.0], [2.5, 493.0], [2.6, 493.0], [2.7, 493.0], [2.8, 493.0], [2.9, 493.0], [3.0, 493.0], [3.1, 493.0], [3.2, 494.0], [3.3, 494.0], [3.4, 494.0], [3.5, 494.0], [3.6, 494.0], [3.7, 494.0], [3.8, 509.0], [3.9, 509.0], [4.0, 509.0], [4.1, 509.0], [4.2, 509.0], [4.3, 509.0], [4.4, 511.0], [4.5, 511.0], [4.6, 511.0], [4.7, 511.0], [4.8, 511.0], [4.9, 511.0], [5.0, 511.0], [5.1, 511.0], [5.2, 511.0], [5.3, 511.0], [5.4, 511.0], [5.5, 511.0], [5.6, 511.0], [5.7, 512.0], [5.8, 512.0], [5.9, 512.0], [6.0, 512.0], [6.1, 512.0], [6.2, 512.0], [6.3, 520.0], [6.4, 520.0], [6.5, 520.0], [6.6, 520.0], [6.7, 520.0], [6.8, 520.0], [6.9, 521.0], [7.0, 521.0], [7.1, 521.0], [7.2, 521.0], [7.3, 521.0], [7.4, 521.0], [7.5, 528.0], [7.6, 528.0], [7.7, 528.0], [7.8, 528.0], [7.9, 528.0], [8.0, 528.0], [8.1, 528.0], [8.2, 528.0], [8.3, 528.0], [8.4, 528.0], [8.5, 528.0], [8.6, 528.0], [8.7, 528.0], [8.8, 536.0], [8.9, 536.0], [9.0, 536.0], [9.1, 536.0], [9.2, 536.0], [9.3, 536.0], [9.4, 546.0], [9.5, 546.0], [9.6, 546.0], [9.7, 546.0], [9.8, 546.0], [9.9, 546.0], [10.0, 550.0], [10.1, 550.0], [10.2, 550.0], [10.3, 550.0], [10.4, 550.0], [10.5, 550.0], [10.6, 550.0], [10.7, 554.0], [10.8, 554.0], [10.9, 554.0], [11.0, 554.0], [11.1, 554.0], [11.2, 554.0], [11.3, 557.0], [11.4, 557.0], [11.5, 557.0], [11.6, 557.0], [11.7, 557.0], [11.8, 557.0], [11.9, 560.0], [12.0, 560.0], [12.1, 560.0], [12.2, 560.0], [12.3, 560.0], [12.4, 560.0], [12.5, 568.0], [12.6, 568.0], [12.7, 568.0], [12.8, 568.0], [12.9, 568.0], [13.0, 568.0], [13.1, 568.0], [13.2, 569.0], [13.3, 569.0], [13.4, 569.0], [13.5, 569.0], [13.6, 569.0], [13.7, 569.0], [13.8, 572.0], [13.9, 572.0], [14.0, 572.0], [14.1, 572.0], [14.2, 572.0], [14.3, 572.0], [14.4, 586.0], [14.5, 586.0], [14.6, 586.0], [14.7, 586.0], [14.8, 586.0], [14.9, 586.0], [15.0, 590.0], [15.1, 590.0], [15.2, 590.0], [15.3, 590.0], [15.4, 590.0], [15.5, 590.0], [15.6, 590.0], [15.7, 596.0], [15.8, 596.0], [15.9, 596.0], [16.0, 596.0], [16.1, 596.0], [16.2, 596.0], [16.3, 598.0], [16.4, 598.0], [16.5, 598.0], [16.6, 598.0], [16.7, 598.0], [16.8, 598.0], [16.9, 598.0], [17.0, 598.0], [17.1, 598.0], [17.2, 598.0], [17.3, 598.0], [17.4, 598.0], [17.5, 603.0], [17.6, 603.0], [17.7, 603.0], [17.8, 603.0], [17.9, 603.0], [18.0, 603.0], [18.1, 603.0], [18.2, 627.0], [18.3, 627.0], [18.4, 627.0], [18.5, 627.0], [18.6, 627.0], [18.7, 627.0], [18.8, 631.0], [18.9, 631.0], [19.0, 631.0], [19.1, 631.0], [19.2, 631.0], [19.3, 631.0], [19.4, 639.0], [19.5, 639.0], [19.6, 639.0], [19.7, 639.0], [19.8, 639.0], [19.9, 639.0], [20.0, 640.0], [20.1, 640.0], [20.2, 640.0], [20.3, 640.0], [20.4, 640.0], [20.5, 640.0], [20.6, 640.0], [20.7, 640.0], [20.8, 640.0], [20.9, 640.0], [21.0, 640.0], [21.1, 640.0], [21.2, 640.0], [21.3, 640.0], [21.4, 640.0], [21.5, 640.0], [21.6, 640.0], [21.7, 640.0], [21.8, 640.0], [21.9, 644.0], [22.0, 644.0], [22.1, 644.0], [22.2, 644.0], [22.3, 644.0], [22.4, 644.0], [22.5, 653.0], [22.6, 653.0], [22.7, 653.0], [22.8, 653.0], [22.9, 653.0], [23.0, 653.0], [23.1, 653.0], [23.2, 655.0], [23.3, 655.0], [23.4, 655.0], [23.5, 655.0], [23.6, 655.0], [23.7, 655.0], [23.8, 661.0], [23.9, 661.0], [24.0, 661.0], [24.1, 661.0], [24.2, 661.0], [24.3, 661.0], [24.4, 663.0], [24.5, 663.0], [24.6, 663.0], [24.7, 663.0], [24.8, 663.0], [24.9, 663.0], [25.0, 666.0], [25.1, 666.0], [25.2, 666.0], [25.3, 666.0], [25.4, 666.0], [25.5, 666.0], [25.6, 666.0], [25.7, 672.0], [25.8, 672.0], [25.9, 672.0], [26.0, 672.0], [26.1, 672.0], [26.2, 672.0], [26.3, 677.0], [26.4, 677.0], [26.5, 677.0], [26.6, 677.0], [26.7, 677.0], [26.8, 677.0], [26.9, 681.0], [27.0, 681.0], [27.1, 681.0], [27.2, 681.0], [27.3, 681.0], [27.4, 681.0], [27.5, 682.0], [27.6, 682.0], [27.7, 682.0], [27.8, 682.0], [27.9, 682.0], [28.0, 682.0], [28.1, 682.0], [28.2, 690.0], [28.3, 690.0], [28.4, 690.0], [28.5, 690.0], [28.6, 690.0], [28.7, 690.0], [28.8, 696.0], [28.9, 696.0], [29.0, 696.0], [29.1, 696.0], [29.2, 696.0], [29.3, 696.0], [29.4, 699.0], [29.5, 699.0], [29.6, 699.0], [29.7, 699.0], [29.8, 699.0], [29.9, 699.0], [30.0, 700.0], [30.1, 700.0], [30.2, 700.0], [30.3, 700.0], [30.4, 700.0], [30.5, 700.0], [30.6, 700.0], [30.7, 701.0], [30.8, 701.0], [30.9, 701.0], [31.0, 701.0], [31.1, 701.0], [31.2, 701.0], [31.3, 716.0], [31.4, 716.0], [31.5, 716.0], [31.6, 716.0], [31.7, 716.0], [31.8, 716.0], [31.9, 722.0], [32.0, 722.0], [32.1, 722.0], [32.2, 722.0], [32.3, 722.0], [32.4, 722.0], [32.5, 722.0], [32.6, 722.0], [32.7, 722.0], [32.8, 722.0], [32.9, 722.0], [33.0, 722.0], [33.1, 722.0], [33.2, 722.0], [33.3, 722.0], [33.4, 722.0], [33.5, 722.0], [33.6, 722.0], [33.7, 722.0], [33.8, 722.0], [33.9, 722.0], [34.0, 722.0], [34.1, 722.0], [34.2, 722.0], [34.3, 722.0], [34.4, 725.0], [34.5, 725.0], [34.6, 725.0], [34.7, 725.0], [34.8, 725.0], [34.9, 725.0], [35.0, 726.0], [35.1, 726.0], [35.2, 726.0], [35.3, 726.0], [35.4, 726.0], [35.5, 726.0], [35.6, 726.0], [35.7, 726.0], [35.8, 726.0], [35.9, 726.0], [36.0, 726.0], [36.1, 726.0], [36.2, 726.0], [36.3, 728.0], [36.4, 728.0], [36.5, 728.0], [36.6, 728.0], [36.7, 728.0], [36.8, 728.0], [36.9, 729.0], [37.0, 729.0], [37.1, 729.0], [37.2, 729.0], [37.3, 729.0], [37.4, 729.0], [37.5, 731.0], [37.6, 731.0], [37.7, 731.0], [37.8, 731.0], [37.9, 731.0], [38.0, 731.0], [38.1, 731.0], [38.2, 739.0], [38.3, 739.0], [38.4, 739.0], [38.5, 739.0], [38.6, 739.0], [38.7, 739.0], [38.8, 743.0], [38.9, 743.0], [39.0, 743.0], [39.1, 743.0], [39.2, 743.0], [39.3, 743.0], [39.4, 744.0], [39.5, 744.0], [39.6, 744.0], [39.7, 744.0], [39.8, 744.0], [39.9, 744.0], [40.0, 747.0], [40.1, 747.0], [40.2, 747.0], [40.3, 747.0], [40.4, 747.0], [40.5, 747.0], [40.6, 747.0], [40.7, 759.0], [40.8, 759.0], [40.9, 759.0], [41.0, 759.0], [41.1, 759.0], [41.2, 759.0], [41.3, 762.0], [41.4, 762.0], [41.5, 762.0], [41.6, 762.0], [41.7, 762.0], [41.8, 762.0], [41.9, 764.0], [42.0, 764.0], [42.1, 764.0], [42.2, 764.0], [42.3, 764.0], [42.4, 764.0], [42.5, 767.0], [42.6, 767.0], [42.7, 767.0], [42.8, 767.0], [42.9, 767.0], [43.0, 767.0], [43.1, 767.0], [43.2, 775.0], [43.3, 775.0], [43.4, 775.0], [43.5, 775.0], [43.6, 775.0], [43.7, 775.0], [43.8, 788.0], [43.9, 788.0], [44.0, 788.0], [44.1, 788.0], [44.2, 788.0], [44.3, 788.0], [44.4, 792.0], [44.5, 792.0], [44.6, 792.0], [44.7, 792.0], [44.8, 792.0], [44.9, 792.0], [45.0, 798.0], [45.1, 798.0], [45.2, 798.0], [45.3, 798.0], [45.4, 798.0], [45.5, 798.0], [45.6, 798.0], [45.7, 803.0], [45.8, 803.0], [45.9, 803.0], [46.0, 803.0], [46.1, 803.0], [46.2, 803.0], [46.3, 804.0], [46.4, 804.0], [46.5, 804.0], [46.6, 804.0], [46.7, 804.0], [46.8, 804.0], [46.9, 808.0], [47.0, 808.0], [47.1, 808.0], [47.2, 808.0], [47.3, 808.0], [47.4, 808.0], [47.5, 821.0], [47.6, 821.0], [47.7, 821.0], [47.8, 821.0], [47.9, 821.0], [48.0, 821.0], [48.1, 821.0], [48.2, 824.0], [48.3, 824.0], [48.4, 824.0], [48.5, 824.0], [48.6, 824.0], [48.7, 824.0], [48.8, 831.0], [48.9, 831.0], [49.0, 831.0], [49.1, 831.0], [49.2, 831.0], [49.3, 831.0], [49.4, 833.0], [49.5, 833.0], [49.6, 833.0], [49.7, 833.0], [49.8, 833.0], [49.9, 833.0], [50.0, 837.0], [50.1, 837.0], [50.2, 837.0], [50.3, 837.0], [50.4, 837.0], [50.5, 837.0], [50.6, 837.0], [50.7, 858.0], [50.8, 858.0], [50.9, 858.0], [51.0, 858.0], [51.1, 858.0], [51.2, 858.0], [51.3, 870.0], [51.4, 870.0], [51.5, 870.0], [51.6, 870.0], [51.7, 870.0], [51.8, 870.0], [51.9, 917.0], [52.0, 917.0], [52.1, 917.0], [52.2, 917.0], [52.3, 917.0], [52.4, 917.0], [52.5, 947.0], [52.6, 947.0], [52.7, 947.0], [52.8, 947.0], [52.9, 947.0], [53.0, 947.0], [53.1, 947.0], [53.2, 1260.0], [53.3, 1260.0], [53.4, 1260.0], [53.5, 1260.0], [53.6, 1260.0], [53.7, 1260.0], [53.8, 1262.0], [53.9, 1262.0], [54.0, 1262.0], [54.1, 1262.0], [54.2, 1262.0], [54.3, 1262.0], [54.4, 1269.0], [54.5, 1269.0], [54.6, 1269.0], [54.7, 1269.0], [54.8, 1269.0], [54.9, 1269.0], [55.0, 1278.0], [55.1, 1278.0], [55.2, 1278.0], [55.3, 1278.0], [55.4, 1278.0], [55.5, 1278.0], [55.6, 1278.0], [55.7, 1282.0], [55.8, 1282.0], [55.9, 1282.0], [56.0, 1282.0], [56.1, 1282.0], [56.2, 1282.0], [56.3, 1285.0], [56.4, 1285.0], [56.5, 1285.0], [56.6, 1285.0], [56.7, 1285.0], [56.8, 1285.0], [56.9, 1286.0], [57.0, 1286.0], [57.1, 1286.0], [57.2, 1286.0], [57.3, 1286.0], [57.4, 1286.0], [57.5, 1288.0], [57.6, 1288.0], [57.7, 1288.0], [57.8, 1288.0], [57.9, 1288.0], [58.0, 1288.0], [58.1, 1288.0], [58.2, 1290.0], [58.3, 1290.0], [58.4, 1290.0], [58.5, 1290.0], [58.6, 1290.0], [58.7, 1290.0], [58.8, 1293.0], [58.9, 1293.0], [59.0, 1293.0], [59.1, 1293.0], [59.2, 1293.0], [59.3, 1293.0], [59.4, 1298.0], [59.5, 1298.0], [59.6, 1298.0], [59.7, 1298.0], [59.8, 1298.0], [59.9, 1298.0], [60.0, 1304.0], [60.1, 1304.0], [60.2, 1304.0], [60.3, 1304.0], [60.4, 1304.0], [60.5, 1304.0], [60.6, 1304.0], [60.7, 1310.0], [60.8, 1310.0], [60.9, 1310.0], [61.0, 1310.0], [61.1, 1310.0], [61.2, 1310.0], [61.3, 1311.0], [61.4, 1311.0], [61.5, 1311.0], [61.6, 1311.0], [61.7, 1311.0], [61.8, 1311.0], [61.9, 1316.0], [62.0, 1316.0], [62.1, 1316.0], [62.2, 1316.0], [62.3, 1316.0], [62.4, 1316.0], [62.5, 1319.0], [62.6, 1319.0], [62.7, 1319.0], [62.8, 1319.0], [62.9, 1319.0], [63.0, 1319.0], [63.1, 1319.0], [63.2, 1323.0], [63.3, 1323.0], [63.4, 1323.0], [63.5, 1323.0], [63.6, 1323.0], [63.7, 1323.0], [63.8, 1325.0], [63.9, 1325.0], [64.0, 1325.0], [64.1, 1325.0], [64.2, 1325.0], [64.3, 1325.0], [64.4, 1328.0], [64.5, 1328.0], [64.6, 1328.0], [64.7, 1328.0], [64.8, 1328.0], [64.9, 1328.0], [65.0, 1328.0], [65.1, 1328.0], [65.2, 1328.0], [65.3, 1328.0], [65.4, 1328.0], [65.5, 1328.0], [65.6, 1328.0], [65.7, 1328.0], [65.8, 1328.0], [65.9, 1328.0], [66.0, 1328.0], [66.1, 1328.0], [66.2, 1328.0], [66.3, 1334.0], [66.4, 1334.0], [66.5, 1334.0], [66.6, 1334.0], [66.7, 1334.0], [66.8, 1334.0], [66.9, 1334.0], [67.0, 1334.0], [67.1, 1334.0], [67.2, 1334.0], [67.3, 1334.0], [67.4, 1334.0], [67.5, 1335.0], [67.6, 1335.0], [67.7, 1335.0], [67.8, 1335.0], [67.9, 1335.0], [68.0, 1335.0], [68.1, 1335.0], [68.2, 1337.0], [68.3, 1337.0], [68.4, 1337.0], [68.5, 1337.0], [68.6, 1337.0], [68.7, 1337.0], [68.8, 1341.0], [68.9, 1341.0], [69.0, 1341.0], [69.1, 1341.0], [69.2, 1341.0], [69.3, 1341.0], [69.4, 1343.0], [69.5, 1343.0], [69.6, 1343.0], [69.7, 1343.0], [69.8, 1343.0], [69.9, 1343.0], [70.0, 1348.0], [70.1, 1348.0], [70.2, 1348.0], [70.3, 1348.0], [70.4, 1348.0], [70.5, 1348.0], [70.6, 1348.0], [70.7, 1356.0], [70.8, 1356.0], [70.9, 1356.0], [71.0, 1356.0], [71.1, 1356.0], [71.2, 1356.0], [71.3, 1377.0], [71.4, 1377.0], [71.5, 1377.0], [71.6, 1377.0], [71.7, 1377.0], [71.8, 1377.0], [71.9, 1381.0], [72.0, 1381.0], [72.1, 1381.0], [72.2, 1381.0], [72.3, 1381.0], [72.4, 1381.0], [72.5, 1388.0], [72.6, 1388.0], [72.7, 1388.0], [72.8, 1388.0], [72.9, 1388.0], [73.0, 1388.0], [73.1, 1388.0], [73.2, 1390.0], [73.3, 1390.0], [73.4, 1390.0], [73.5, 1390.0], [73.6, 1390.0], [73.7, 1390.0], [73.8, 1390.0], [73.9, 1390.0], [74.0, 1390.0], [74.1, 1390.0], [74.2, 1390.0], [74.3, 1390.0], [74.4, 1393.0], [74.5, 1393.0], [74.6, 1393.0], [74.7, 1393.0], [74.8, 1393.0], [74.9, 1393.0], [75.0, 1398.0], [75.1, 1398.0], [75.2, 1398.0], [75.3, 1398.0], [75.4, 1398.0], [75.5, 1398.0], [75.6, 1398.0], [75.7, 1399.0], [75.8, 1399.0], [75.9, 1399.0], [76.0, 1399.0], [76.1, 1399.0], [76.2, 1399.0], [76.3, 1413.0], [76.4, 1413.0], [76.5, 1413.0], [76.6, 1413.0], [76.7, 1413.0], [76.8, 1413.0], [76.9, 1432.0], [77.0, 1432.0], [77.1, 1432.0], [77.2, 1432.0], [77.3, 1432.0], [77.4, 1432.0], [77.5, 1436.0], [77.6, 1436.0], [77.7, 1436.0], [77.8, 1436.0], [77.9, 1436.0], [78.0, 1436.0], [78.1, 1436.0], [78.2, 1437.0], [78.3, 1437.0], [78.4, 1437.0], [78.5, 1437.0], [78.6, 1437.0], [78.7, 1437.0], [78.8, 1438.0], [78.9, 1438.0], [79.0, 1438.0], [79.1, 1438.0], [79.2, 1438.0], [79.3, 1438.0], [79.4, 1441.0], [79.5, 1441.0], [79.6, 1441.0], [79.7, 1441.0], [79.8, 1441.0], [79.9, 1441.0], [80.0, 1461.0], [80.1, 1461.0], [80.2, 1461.0], [80.3, 1461.0], [80.4, 1461.0], [80.5, 1461.0], [80.6, 1461.0], [80.7, 1482.0], [80.8, 1482.0], [80.9, 1482.0], [81.0, 1482.0], [81.1, 1482.0], [81.2, 1482.0], [81.3, 1494.0], [81.4, 1494.0], [81.5, 1494.0], [81.6, 1494.0], [81.7, 1494.0], [81.8, 1494.0], [81.9, 1495.0], [82.0, 1495.0], [82.1, 1495.0], [82.2, 1495.0], [82.3, 1495.0], [82.4, 1495.0], [82.5, 1504.0], [82.6, 1504.0], [82.7, 1504.0], [82.8, 1504.0], [82.9, 1504.0], [83.0, 1504.0], [83.1, 1504.0], [83.2, 1508.0], [83.3, 1508.0], [83.4, 1508.0], [83.5, 1508.0], [83.6, 1508.0], [83.7, 1508.0], [83.8, 1512.0], [83.9, 1512.0], [84.0, 1512.0], [84.1, 1512.0], [84.2, 1512.0], [84.3, 1512.0], [84.4, 1527.0], [84.5, 1527.0], [84.6, 1527.0], [84.7, 1527.0], [84.8, 1527.0], [84.9, 1527.0], [85.0, 1528.0], [85.1, 1528.0], [85.2, 1528.0], [85.3, 1528.0], [85.4, 1528.0], [85.5, 1528.0], [85.6, 1528.0], [85.7, 1530.0], [85.8, 1530.0], [85.9, 1530.0], [86.0, 1530.0], [86.1, 1530.0], [86.2, 1530.0], [86.3, 1540.0], [86.4, 1540.0], [86.5, 1540.0], [86.6, 1540.0], [86.7, 1540.0], [86.8, 1540.0], [86.9, 1545.0], [87.0, 1545.0], [87.1, 1545.0], [87.2, 1545.0], [87.3, 1545.0], [87.4, 1545.0], [87.5, 1550.0], [87.6, 1550.0], [87.7, 1550.0], [87.8, 1550.0], [87.9, 1550.0], [88.0, 1550.0], [88.1, 1550.0], [88.2, 1562.0], [88.3, 1562.0], [88.4, 1562.0], [88.5, 1562.0], [88.6, 1562.0], [88.7, 1562.0], [88.8, 1575.0], [88.9, 1575.0], [89.0, 1575.0], [89.1, 1575.0], [89.2, 1575.0], [89.3, 1575.0], [89.4, 1590.0], [89.5, 1590.0], [89.6, 1590.0], [89.7, 1590.0], [89.8, 1590.0], [89.9, 1590.0], [90.0, 1597.0], [90.1, 1597.0], [90.2, 1597.0], [90.3, 1597.0], [90.4, 1597.0], [90.5, 1597.0], [90.6, 1597.0], [90.7, 1605.0], [90.8, 1605.0], [90.9, 1605.0], [91.0, 1605.0], [91.1, 1605.0], [91.2, 1605.0], [91.3, 1606.0], [91.4, 1606.0], [91.5, 1606.0], [91.6, 1606.0], [91.7, 1606.0], [91.8, 1606.0], [91.9, 1609.0], [92.0, 1609.0], [92.1, 1609.0], [92.2, 1609.0], [92.3, 1609.0], [92.4, 1609.0], [92.5, 1620.0], [92.6, 1620.0], [92.7, 1620.0], [92.8, 1620.0], [92.9, 1620.0], [93.0, 1620.0], [93.1, 1620.0], [93.2, 1680.0], [93.3, 1680.0], [93.4, 1680.0], [93.5, 1680.0], [93.6, 1680.0], [93.7, 1680.0], [93.8, 1705.0], [93.9, 1705.0], [94.0, 1705.0], [94.1, 1705.0], [94.2, 1705.0], [94.3, 1705.0], [94.4, 1734.0], [94.5, 1734.0], [94.6, 1734.0], [94.7, 1734.0], [94.8, 1734.0], [94.9, 1734.0], [95.0, 1750.0], [95.1, 1750.0], [95.2, 1750.0], [95.3, 1750.0], [95.4, 1750.0], [95.5, 1750.0], [95.6, 1750.0], [95.7, 1793.0], [95.8, 1793.0], [95.9, 1793.0], [96.0, 1793.0], [96.1, 1793.0], [96.2, 1793.0], [96.3, 1954.0], [96.4, 1954.0], [96.5, 1954.0], [96.6, 1954.0], [96.7, 1954.0], [96.8, 1954.0], [96.9, 1990.0], [97.0, 1990.0], [97.1, 1990.0], [97.2, 1990.0], [97.3, 1990.0], [97.4, 1990.0], [97.5, 1998.0], [97.6, 1998.0], [97.7, 1998.0], [97.8, 1998.0], [97.9, 1998.0], [98.0, 1998.0], [98.1, 1998.0], [98.2, 2009.0], [98.3, 2009.0], [98.4, 2009.0], [98.5, 2009.0], [98.6, 2009.0], [98.7, 2009.0], [98.8, 2072.0], [98.9, 2072.0], [99.0, 2072.0], [99.1, 2072.0], [99.2, 2072.0], [99.3, 2072.0], [99.4, 2155.0], [99.5, 2155.0], [99.6, 2155.0], [99.7, 2155.0], [99.8, 2155.0], [99.9, 2155.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 400.0, "maxY": 26.0, "series": [{"data": [[2100.0, 1.0], [600.0, 20.0], [700.0, 25.0], [800.0, 10.0], [900.0, 2.0], [1200.0, 11.0], [1300.0, 26.0], [1400.0, 10.0], [1500.0, 13.0], [400.0, 6.0], [1600.0, 5.0], [1700.0, 4.0], [1900.0, 3.0], [2000.0, 2.0], [500.0, 22.0]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 2100.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 6.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 126.0, "series": [{"data": [[0.0, 6.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 126.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 28.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 6.500000000000001, "minX": 1.64476284E12, "maxY": 8.0, "series": [{"data": [[1.64476284E12, 8.0], [1.6447629E12, 6.500000000000001]], "isOverall": false, "label": "Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6447629E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 528.0, "minX": 1.0, "maxY": 1441.0, "series": [{"data": [[8.0, 1064.300699300699], [4.0, 528.0], [2.0, 1388.0], [1.0, 1441.0], [5.0, 675.5], [6.0, 925.3333333333334], [3.0, 1356.0], [7.0, 851.25]], "isOverall": false, "label": "HTTP Request", "isController": false}, {"data": [[7.7375, 1049.0312499999998]], "isOverall": false, "label": "HTTP Request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 8.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 58.8, "minX": 1.64476284E12, "maxY": 17036.466666666667, "series": [{"data": [[1.64476284E12, 17036.466666666667], [1.6447629E12, 3406.2]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.64476284E12, 277.2], [1.6447629E12, 58.8]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6447629E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 979.3214285714288, "minX": 1.64476284E12, "maxY": 1063.8181818181818, "series": [{"data": [[1.64476284E12, 1063.8181818181818], [1.6447629E12, 979.3214285714288]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6447629E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 978.1428571428573, "minX": 1.64476284E12, "maxY": 1060.0757575757582, "series": [{"data": [[1.64476284E12, 1060.0757575757582], [1.6447629E12, 978.1428571428573]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6447629E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 396.21428571428567, "minX": 1.64476284E12, "maxY": 412.2121212121211, "series": [{"data": [[1.64476284E12, 412.2121212121211], [1.6447629E12, 396.21428571428567]], "isOverall": false, "label": "HTTP Request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6447629E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 449.0, "minX": 1.64476284E12, "maxY": 2155.0, "series": [{"data": [[1.64476284E12, 2155.0], [1.6447629E12, 1734.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.64476284E12, 449.0], [1.6447629E12, 453.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.64476284E12, 1605.7], [1.6447629E12, 1508.1000000000001]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.64476284E12, 2127.6099999999988], [1.6447629E12, 1734.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.64476284E12, 1849.349999999999], [1.6447629E12, 1648.9499999999994]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6447629E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 732.5, "minX": 1.0, "maxY": 1441.0, "series": [{"data": [[4.0, 1319.5], [8.0, 1368.0], [2.0, 1372.0], [1.0, 1441.0], [9.0, 1393.0], [10.0, 845.5], [5.0, 788.0], [6.0, 953.0], [12.0, 732.5], [7.0, 803.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 12.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 728.5, "minX": 1.0, "maxY": 1440.0, "series": [{"data": [[4.0, 1318.0], [8.0, 1367.0], [2.0, 1371.5], [1.0, 1440.0], [9.0, 1360.0], [10.0, 845.0], [5.0, 787.0], [6.0, 952.0], [12.0, 728.5], [7.0, 799.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 12.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.3333333333333333, "minX": 1.64476284E12, "maxY": 2.3333333333333335, "series": [{"data": [[1.64476284E12, 2.3333333333333335], [1.6447629E12, 0.3333333333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6447629E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.4666666666666667, "minX": 1.64476284E12, "maxY": 2.2, "series": [{"data": [[1.64476284E12, 2.2], [1.6447629E12, 0.4666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6447629E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.4666666666666667, "minX": 1.64476284E12, "maxY": 2.2, "series": [{"data": [[1.64476284E12, 2.2], [1.6447629E12, 0.4666666666666667]], "isOverall": false, "label": "HTTP Request-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6447629E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.4666666666666667, "minX": 1.64476284E12, "maxY": 2.2, "series": [{"data": [[1.64476284E12, 2.2], [1.6447629E12, 0.4666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6447629E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 19800000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

