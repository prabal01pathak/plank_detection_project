<h1 align="center">Welcome to plank_detection 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
</p>

> Plank Detection : To detect plank with machine learning models

### 🏠 [Homepage](Plank Detection)

## Install

```sh
pip install -r requirements.txt
```

## Usage

```sh
uvicorn src.main:app --host 0.0.0.0 --port 8000 --reload
```

## Author

👤 **Prabal Pathak**

-   Website: https://prabals.herokuapp.com
-   Github: [@prabal01pathak](https://github.com/prabal01pathak)
-   Gitlab: [@prabal01pathak](https://gitlab.com/prabal01pathak)

## Show your support

Give a ⭐️ if this project helped you!
